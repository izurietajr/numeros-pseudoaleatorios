package proy;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Border;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Alert {

    public static void show(String title, String message){
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle(title);
        stage.setResizable(false);
        //stage.setMinWidth(300);
        //stage.setMinHeight(200);

        Label lblMsg = new Label(message);
        lblMsg.setAlignment(Pos.CENTER);
        Button btnOk = new Button("Ok");
        btnOk.setOnAction(e -> stage.close());

        VBox layout = new VBox(20);
        layout.getChildren().addAll(lblMsg, btnOk);
        layout.setAlignment(Pos.CENTER);
        layout.setPadding(new Insets(20, 40, 20, 40));

        Scene scene = new Scene(layout);
        stage.setScene(scene);
        stage.showAndWait();
    }

}
