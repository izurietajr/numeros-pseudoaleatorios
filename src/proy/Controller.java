package proy;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;


public class Controller {

    private int selectedMethod = Method.CUADRADOS_MEDIOS;
    @FXML
    private TextField txtSeed;
    @FXML
    private TextField txtMultiplier;
    @FXML
    private TextField txtMod;
    @FXML
    private TextField txtConst;
    @FXML
    private Label lblExp;
    @FXML
    private RadioButton radCuad;
    @FXML
    private RadioButton radMix;
    @FXML
    private RadioButton radBin;
    @FXML
    private RadioButton radMult;
    @FXML
    private TableView<Number> numbersTable;
    @FXML
    private TableColumn<Number, String> colPosition;
    @FXML
    private TableColumn<Number, String> colNumber;
    @FXML
    private TableColumn<Number, String> colU;
    @FXML
    private Label lblPeriod;

    @FXML
    private void initialize(){
        System.out.println("initial configuration");
        numbersTable.setPlaceholder(new Label("Aún no se ha generado ningún número"));
        // TODO set initial config
    }

    public void Calculate(){

        if(Validate(txtSeed) && Validate(txtMultiplier) && Validate(txtMod) && Validate(txtConst))
        {
            RandomNumbers rand = new RandomNumbers(
                    Integer.parseInt(txtSeed.getText()),
                    Integer.parseInt(txtMultiplier.getText()),
                    Integer.parseInt(txtMod.getText()),
                    Integer.parseInt(txtConst.getText()),
                    selectedMethod
            );
            if(rand.getValid())
            {
                lblExp.setText(rand.getExpresion());
                ObservableList<Number> numbersList = FXCollections.observableArrayList(rand.getNumbers());
                numbersTable.setItems(numbersList);
                colPosition.setCellValueFactory(cellData -> cellData.getValue().positionProperty());
                colNumber.setCellValueFactory(cellData -> cellData.getValue().valueProperty());
                colU.setCellValueFactory(cellData -> cellData.getValue().uProperty());
                lblPeriod.setText("Periodo: " + rand.getPeriod());
            }
            else
            {
                System.out.println("Los datos de entrada no son válidos"); // TODO: usar un Alert
            }
        }

    }

    public void RadioClicked(){

        if (radCuad.isSelected()){
            selectedMethod = Method.CUADRADOS_MEDIOS;
            ableDisableTextFields(true, true, true);
        }
        if (radMix.isSelected()){
            selectedMethod = Method.CONGRUENCIAL_MIXTO;
            ableDisableTextFields(false, false, false);

        }
        if (radMult.isSelected()){
            selectedMethod = Method.CONGRUENCIAL_MULTIPLICATIVO;
            ableDisableTextFields(false, false, true);

        }
        if (radBin.isSelected()){
            selectedMethod = Method.CONGRUENCIAL_MULT_BINARIO;
            ableDisableTextFields(false, false, true);

        }

    }

    private void ableDisableTextFields(Boolean multiplier, Boolean module, Boolean constant){
        txtSeed.setDisable(false);
        txtMultiplier.setDisable(multiplier);
        txtMod.setDisable(module);
        txtConst.setDisable(constant);
    }

    private Boolean Validate(TextField text){
        try
        {
            Integer.parseInt(text.getText());
            text.setStyle("-fx-max-width: 100;");
            return true;
        }catch (NumberFormatException e){
            text.setStyle("-fx-border-color: red; -fx-max-width: 100; -fx-border-radius: 3px");
            Alert.show("Error", "El valor \""+text.getText()+"\" no es válido");
            return false;
        }
    }

    public void ShowAbout(){
        Alert.show("About", "Proyecto INF 391\n" +
                "Métodos de generación de números pseudoaleatorios\n" +
                "\n" +
                "Integrantes:\n" +
                "· Espinoza Apaza Wendy Susan\n" +
                "· Guarachi Enriquez Ronald Alcides\n" +
                "· Helguero Vargas Pablo Vicente\n" +
                "· Izurieta Veliz Jesus Rodolfo\n" +
                "· Soria Soria Rubén Gonzalo\n" +
                "\n" +
                "Código fuente:\n" +
                "gitlab.com/izurietajr/numeros-pseudoaleatorios");
    }
}
