package proy;

import javafx.beans.property.*;

import java.util.ArrayList;
import java.util.List;

public class RandomNumbers {

    private int seed;
    private int multiplier;
    private int module;
    private int constant;
    private int period;
    private int method;
    private List<Number> numbers;
    private Boolean valid;

    RandomNumbers(int seed, int multiplier, int module, int constant, int method){
        this.seed = seed;
        this.multiplier = multiplier;
        this.module = module;
        this.constant = constant;
        this.method = method;
        this.numbers = new ArrayList<Number>();
        this.valid = false;
        if(isValid()){
            Calculate();
        }
    }

    private void Calculate()
    {
        if(method == Method.CUADRADOS_MEDIOS) CuadradosMedios();
        if(method == Method.CONGRUENCIAL_MIXTO) CongruencialMixto();
        if(method == Method.CONGRUENCIAL_MULTIPLICATIVO) CongruencialMultiplicativo();
        if(method == Method.CONGRUENCIAL_MULT_BINARIO) CongruencialMultBinario();
    }

    public Boolean isValid()
    {
        // TODO: validar de acuerdo al método
        valid = true;
        String message = "";

        if(seed <= 0){
            message += "La semilla debe ser mayor a 0\n";
            valid = false;
        }
        if(multiplier <= 0 && (method == Method.CONGRUENCIAL_MIXTO || method == Method.CONGRUENCIAL_MULTIPLICATIVO || method == Method.CONGRUENCIAL_MULT_BINARIO)){
            message += "El multiplicador debe ser mayor a 0\n";
            valid = false;
        }
        if(module <= 0 && (method == Method.CONGRUENCIAL_MIXTO || method == Method.CONGRUENCIAL_MULTIPLICATIVO || method == Method.CONGRUENCIAL_MULT_BINARIO)){
            message += "El módulo debe ser mayor a 0\n";
            valid = false;
        }
        if(constant <= 0 && method == Method.CONGRUENCIAL_MIXTO){
            message += "La constante debe ser mayor a 0\n";
            valid = false;
        }

        if(method == Method.CUADRADOS_MEDIOS){
            if((int)(Math.log10(seed)+1)<=3){
                message += "La semilla debe tener al menos 4 cifras\n";
                valid = false;
            }
        }

        if(method == Method.CONGRUENCIAL_MIXTO){
            if(module <= seed || module <= multiplier || module <= constant){
                message += "El módulo debe ser mayor a la semilla, al multiplicador y a la constante\n";
                valid = false;
            }
        }

        if(method == Method.CONGRUENCIAL_MULTIPLICATIVO){
            if(seed % 5 == 0){
                message += "La semilla no debe ser múltiplo de 5\n";
                valid = false;
            }
            if(seed % 2 == 0){
                message += "La semilla no debe ser múltiplo de 2\n";
                valid = false;
            }
        }

        if(method == Method.CONGRUENCIAL_MULT_BINARIO){
            if (seed % 2 == 0){
                message += "La semilla debe ser impar\n";
                valid = false;
            }
            if ((multiplier+3) % 8 != 0 && (multiplier-3) % 8 != 0){
                message += "El multiplicador debe ser de la forma:\n \"8t\u00b13\" para t entero\n";
                valid = false;
            }
        }

        if(!valid) Alert.show("Valores no válidos", message);

        return valid;
    }

    private void CuadradosMedios(){

        period = 0;
        List<Integer> lista = new ArrayList<>();
        int p = ((int) Math.log10(seed) + 1);
        int num = seed;
        Boolean next = true;
        while(next){
            period ++;
            num = (int) Math.pow(num, 2);
            num = Integer.parseInt(GetCenterNumber(num, p));
            if(num == 0 || lista.contains(num)){ next = false;}
            lista.add(num);
            numbers.add(new Number(period, Double.parseDouble(num+""),num/Math.pow(10, (p))));
        }
        period --;
    }

    private String GetCenterNumber(int num, int size){

        String numstr = num+"";
        int numsize = numstr.length();
        if (numsize % 2 != 0){
            numstr = "0" + numstr;
            numsize ++;
        }
        int init = (numsize-size)/2;
        return numstr.substring(init, init+size);
    }

    private void CongruencialMixto(){
        period = module;
        int per = 0;
        Boolean next = true;
        List<Double> lista = new ArrayList<>();
        double ui;
        double xN = seed;
        double xn = 0;
        while(next){
            per++;
            xn = xN;
            xN = (multiplier * xN + constant) % module;
            ui = xN / module;
            numbers.add(new Number(per, xn, ui));
            if(lista.contains(xn)){ next = false;}
            lista.add(xn);
        }
        period = --per;
    }

    private void CongruencialMultiplicativo(){

        int per = 0;
        Boolean next = true;
        List<Double> lista = new ArrayList<>();

        double xN = seed;
        while(next){
            per++;
            xN = (multiplier * xN) % module;
            numbers.add(new Number(per, xN, xN/module));
            if(lista.contains(xN)){ next = false;}
            lista.add(xN);
        }
        period = --per;
    }

    private void CongruencialMultBinario(){
        int d = (int) (Math.log10(module) / Math.log10(2));
        period = (int) Math.pow(2, d - 2);
        double xN = seed;
        for (int i = 0; i < period+1; i++) {
            xN = (multiplier * xN) % module;
            numbers.add(new Number(i+1, xN, xN/module));
        }
    }

    public String getExpresion(){
        String expresion = "";
        if(method == Method.CUADRADOS_MEDIOS){}
        if(method == Method.CONGRUENCIAL_MIXTO){
            expresion = "x\u2081 = ("+multiplier+"*"+seed+"+"+constant+") mod "+module+"; u\u2081 = x\u2081/"+module;
        }
        if(method == Method.CONGRUENCIAL_MULTIPLICATIVO){
            expresion = "x\u2081 = ("+multiplier+"*"+seed+") mod "+module+"; u\u2081 = x\u2081/"+module;
        }
        if(method == Method.CONGRUENCIAL_MULT_BINARIO){
            expresion = "x\u2081 = ("+multiplier+"*"+seed+") mod "+module+"; u\u2081 = x\u2081/"+module;
        }
        return expresion;
    }

    public Boolean getValid() {
        return valid;
    }

    public int getSeed() {
        return seed;
    }

    public int getMultiplier() {
        return multiplier;
    }

    public int getModule() {
        return module;
    }

    public int getConstant() {
        return constant;
    }

    public int getPeriod(){
        return period;
    }

    public List<Number> getNumbers() {
        return numbers;
    }

    public int getMethod() {
        return method;
    }
}

class Number {
    private IntegerProperty position;
    private DoubleProperty value;
    private DoubleProperty u;

    public Number(int position, Double value, Double u){
        this.position = new SimpleIntegerProperty(position);
        this.value = new SimpleDoubleProperty(value);
        this.u = new SimpleDoubleProperty(u);
    }

    public int getPosition() {
        return position.get();
    }

    public Double getValue() {
        return value.get();
    }

    public Double getU() {
        return u.get();
    }

    public StringProperty positionProperty() {
        return new SimpleStringProperty(position.getValue().toString());
    }

    public StringProperty valueProperty() {
        return new SimpleStringProperty(value.getValue().toString());
    }

    public StringProperty uProperty() {
        return new SimpleStringProperty(u.getValue().toString());
    }
}

class Method {
    static final int CUADRADOS_MEDIOS = 0;
    static final int CONGRUENCIAL_MIXTO = 1;
    static final int CONGRUENCIAL_MULTIPLICATIVO = 2;
    static final int CONGRUENCIAL_MULT_BINARIO = 3;
}
